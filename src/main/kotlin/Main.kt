import java.awt.Color.cyan
import java.util.Scanner

fun main() {
    var retry = true
    var ronda = 0
    while (retry == true) {
        /* Conjunto de variables que utilizaremos en WORDLE */
        val listado = arrayOf(
            "SUSTO",
            "TARTA",
            "RIERA",
            "TRAMA",
            "CARRO",
            "CALLE",
            "FOLIO",
            "PASTA",
            "CABRA",
            "TARRO",
            "DOBLE",
            "TRUCO",
            "AUDIO",
            "AVION",
            "PARTO",
            "CASAL",
            "CALDO",
            "CELDA",
            "VUELO",
            "COSTA",
            "CUERO",
            "POETA",
            "BALON",
            "SOBRE",
            "GUSTO",
            "PASTO",
            "SOBRE",
            "SABOR",
            "TIMBA",
            "SABLE",
            "GASTO"
        )
        var word = "CASAL"
        var wordarray = arrayOf("o", "o", "o", "o", "o")
        var inputarray = arrayOf("o", "o", "o", "o", "o")
        var positionInputArray = 0
        var positionWordArray = 0
        val colorPredeterminat = "\u001b[0m"
        val grey = "\u001B[37m";
        val yellow = "\u001B[33m";
        val green = "\u001B[32m";
        val purple = "\u001B[35m";
        val red = "\u001b[31m";
        val blue = "\u001b[34m";
        val cyan = "\u001b[36m";
        var acierto = 0
        var retryChar = false
        var verde = false
        var amarillo = false
        var error = true
        var errorSumatori = 0
        var letra = "0"
        var caracterIncorrecte = true
        var caractertCorrecte = 0
        var intentos = 0
        ronda++
        wordarray= wordToArray(word)
        instructions(ronda)
        while (intentos < 6 && acierto < 5) {
            println()
            error = true
            caracterIncorrecte = true
            var intentosRestantes = 6 - intentos
            headerMessage(intentos)
            val sc = Scanner(System.`in`)
            println(colorPredeterminat + "Introduce la palabra: ")
            var inputAttempt = sc.next()
            inputarray = correctWord(inputAttempt)
            intentos++
            acierto= checkResult(inputarray, wordarray)
            }
        println()
        retry= result(acierto, intentos, word)
        }
    }


fun wordToArray (word: String): Array<String>{
    var wordarray = arrayOf("o", "o", "o", "o", "o")
    var positionInputArray = 0
    for (x in word) {
        wordarray[positionInputArray] = x.toString()
        positionInputArray++
    }
    return wordarray
}

fun instructions (ronda: Int){
    val colorPredeterminat = "\u001b[0m"
    val grey = "\u001B[37m";
    val yellow = "\u001B[33m";
    val green = "\u001B[32m";
    val purple = "\u001B[35m";
    val red = "\u001b[31m";
    val blue = "\u001b[34m";
    val cyan = "\u001b[36m";
    if (ronda == 1){
        println(cyan + "BIENVENIDOS JUGADORES !")
        print(colorPredeterminat + "Estais  a punto de jugar a ");print(purple + "WORDLE");println(
            colorPredeterminat + ", un juego en el que tendrás que explorar en tu vocabulario para dar con la palabra correcta."
        );println(
            "Te estarás preguntando como funciona, tranquilo vamos a explicarte las normas."
        )
        println()
        print("Las reglas son muy sencillas, tendrás ");print(blue + "6 intentos ");print(colorPredeterminat + "para acertar la palabra oculta que consta de ");print(
            red + "5 letras"
        );println(colorPredeterminat + ".")
        print("Para hacerlo, deberás escribir la palabra, ya sea en ");print(green + "minúsculas");print(
            colorPredeterminat + " o "
        );print(green + "mayúsculas");print(colorPredeterminat + ", ");print(red + "sin espacio en blanco ");println(
            colorPredeterminat + "entre letras."
        )
        println("Cuando lo hayas hecho, pueden ocurrir 3 cosas:");println()
        print("· Si la letra es de color "); print(green + "VERDE");print(colorPredeterminat + " significa que la letra está");print(
            purple + " incluida "
        );print(colorPredeterminat + "en la palabra y en esa ");print(purple + "misma posición");println(
            colorPredeterminat + "."
        );println()
        print("· Si la letra es de color "); print(yellow + "AMARILLO");print(colorPredeterminat + " significa que la letra está");print(
            purple + " incluida "
        );print(colorPredeterminat + "en la palabra pero en ");print(red + "otra posición");println(
            colorPredeterminat + "."
        );println()
        print("· Si la letra es de color ");print(grey + "GRIS");print(colorPredeterminat + " significa que la letra");print(
            red + " no está incluida "
        );println(colorPredeterminat + "en la palabra.");println()
        print("Bueno, qué te parece ? Todo listo ? Mucha suerte y a disfrutar de ");print(purple + "WORDLE");println(
            colorPredeterminat + "!!"
        );println()
    }
    else println(colorPredeterminat + "No hace falta instrucciones verdad ? Confío en tu memoria !");println(purple + "BUENA SUERTE !")
}

fun headerMessage (intentos:Int){
    val colorPredeterminat = "\u001b[0m"
    var intentosRestantes = 6 - intentos
    when (intentos){
        1 -> println(colorPredeterminat + "Que esperabas, acertar a la primera ? No te hundas, esto acaba de empezar. Aún tienes $intentosRestantes intentos.");
        2 -> println(colorPredeterminat + "Otra vez por aqui ? Vaya, esta vez confiaba en ti. No pasa nada aún tienes $intentosRestantes intentos.");
        3 -> println(colorPredeterminat + "Eres una persona a la que le gustan los refranes, por lo de no hay dos sin tres me refiero. Intentos restantes: $intentosRestantes.");
        4 -> println(colorPredeterminat + "Empiezo a pensar que me has cogido cariño. Yo tambien, no lo niego. Te quedan $intentosRestantes intentos.");
        5 -> println(colorPredeterminat + "Vale, lo tuyo es obsesión. Último intento, gana la partida. Lo nuestro no es posible...")
    }

}
fun correctWord(inputAttempt: String): Array<String> {
    var inputarray = arrayOf("o", "o", "o", "o", "o")
    val colorPredeterminat = "\u001b[0m"
    val purple = "\u001B[35m";
    val red = "\u001b[31m";
    val green = "\u001B[32m";
    var caracterIncorrecte = true
    var caractertCorrecte = 0
    var error = true
    var errorSumatori= -1
    var positionInputArray = 0
    val sc = Scanner(System.`in`)
    println(colorPredeterminat + "Introduce la palabra: ")
    var inputAttempt = inputAttempt
    while (error == true || caracterIncorrecte == true) {
        error = true
        errorSumatori++
        if (inputAttempt.length != 5 && errorSumatori < 3) {
            print("Vaya, parece que no sabemos contar...");print(red + "5 LETRAS");println(colorPredeterminat + "!!")
            inputAttempt = sc.next()
            continue
        } else if (inputAttempt.length != 5 && errorSumatori == 3) {
            print("Vale, definitivamente no sabes contar. Voy a ayudarte con un ejemplo...");print(red + "E");print(
                colorPredeterminat + ", uno. "
            );print(red + "R");print(colorPredeterminat + ", dos. ");print(red + "R");print(colorPredeterminat + ", tres. ");print(
                red + "O"
            );print(colorPredeterminat + ", cuatro. ");print(red + "R");println(colorPredeterminat + ", cinco. Prueba ahora !")
            inputAttempt = sc.next()
            continue
        } else if (inputAttempt.length != 5 && errorSumatori > 3) {
            positionInputArray = 0
            for (x in inputAttempt) {
                positionInputArray++
                print(red + "$x");print(colorPredeterminat + ", $positionInputArray. ")
            }
            println()
            print(purple + "$positionInputArray");print(colorPredeterminat + " es diferente de ");print(green + "5"); println(
                colorPredeterminat + " ;)"
            )
            inputAttempt = sc.next()
            continue
        } else {
            error = false;
        }
        positionInputArray = 0
        caractertCorrecte = 0
        /* Con este 'for' buscamos transformar las minúsculas a mayúsculas y detectar si el usuario ha introducido un carácter no permitido.
       Tambien, pasamos dicha palabra a un array de letras. */
        for (letra in inputAttempt) {
            if (letra.hashCode() in 65..90) {
                inputarray[positionInputArray] = letra.toString()
                positionInputArray++; caractertCorrecte++
            } else if (letra.hashCode() in 97..122) {
                inputarray[positionInputArray] = letra.uppercase().toString()
                positionInputArray++; caractertCorrecte++
            } else {
                caractertCorrecte = 0
                print("Recuerda, has de escribir ");print(purple + "LETRAS"); println(colorPredeterminat + " tanto en mayúsculas como minúsculas. Prueba de nuevo!")
                inputAttempt = sc.next()
                continue
            }
        }
        if (caractertCorrecte == 5) {
            caracterIncorrecte = false
        }

    }
    return inputarray
}

fun checkResult(inputarray: Array<String>, wordarray: Array<String>):Int{
    var acierto = 0
    var positionInputArray = -1
    var positionWordArray = 0
    val green = "\u001B[32m";
    val grey = "\u001B[37m";
    val yellow = "\u001B[33m";
    var verde = false
    var amarillo = false
    var gris= false
    var colors = arrayOf("o", "o", "o", "o", "o")
    for (x in inputarray) {
        positionWordArray = -1
        positionInputArray++
        gris= false
        amarillo = false
        verde = false
        for (i in wordarray) {
            positionWordArray++
            if (x == i && positionInputArray != positionWordArray) {
                if (x == wordarray[positionInputArray]) {
                    colors[positionInputArray]= "$x green"
                    verde = true
                    print(green + x)
                    acierto++
                    break
                } else {
                    var numberOfCharInWord=0
                    for (r in wordarray){
                        if (r == x){
                            numberOfCharInWord++
                        }
                    }
                    var correct= 0
                    for (p in colors){
                        if (p == "$x green" || p == "$x yellow"){
                            correct++
                        }
                    }
                    var numberOfCharInInput=0
                    for (g in inputarray){
                        if (g == x){
                            numberOfCharInInput++
                        }
                    }
                    if (correct== numberOfCharInWord){
                        gris= true
                        colors[positionInputArray]= "$x grey"
                        print(grey + x)
                    }
                    else {
                        colors[positionInputArray]= "$x yellow"
                        amarillo = true
                        print(yellow + x)
                    }

                }
                break
            } else if (x == i && positionInputArray == positionWordArray && positionInputArray <= 3) {
                colors[positionInputArray]= "$x green"
                verde = true
                print(green + x)
                acierto++
                break
            } else if (x == i && positionInputArray == positionWordArray && positionInputArray == 4) {
                colors[positionInputArray]= "$x green"
                verde = true
                print(green + x)
                acierto++
                break
            }
        }
        if (amarillo == false && verde == false && gris== false && positionInputArray <= 3) {
            colors[positionInputArray]= "$x grey"
            print(grey + x)
        } else if (amarillo == false && verde == false && gris==false && positionInputArray == 4) {
            colors[positionInputArray]= "$x grey"
            print(grey + x)
        }
    }
    for (r in wordarray){
        for (g in colors){

        }
        if (r == x){
            numberOfCharInWord++
        }
    }
    var correct= 0
    for (p in colors){
        if (p == "$x green" || p == "$x yellow"){
            correct++
        }
    }
    var numberOfCharInInput=0
    for (g in inputarray){
        if (g == x){
            numberOfCharInInput++
        }
    }
    return acierto
}

fun result(acierto: Int, intentos: Int, word: String): Boolean {
    val colorPredeterminat = "\u001b[0m"
    val grey = "\u001B[37m";
    val yellow = "\u001B[33m";
    val green = "\u001B[32m";
    val purple = "\u001B[35m";
    val red = "\u001b[31m";
    val blue = "\u001b[34m";
    val cyan = "\u001b[36m";
    var retry = true
    var ronda= 0
    var retryChar= false
    if (acierto == 5) {
        when (intentos) {
            1 -> println(colorPredeterminat + "Magistral, lo tuyo no es normal. Ves a comprar la lotería, hoy es tu dia de suerte !!");
            2 -> println(colorPredeterminat + "Increíble, tus padres estarán orgullosos. Corre a decírselo. Aunque, espera! No deberías estar estudiando ? ")
            3 -> println(colorPredeterminat + "Sabía que lo lograrías, nunca dejé de confiar. Eres alucinante!")
            4 -> println(colorPredeterminat + "A la cuarta va la vencida, o era a la tercera ? Qué importa, sabía que te gustaban los refranes. Enhorabuena!")
            5 -> println(colorPredeterminat + "Correcto! Me has roto el corazón...ahora que empezaba a quererte.Lo entiendo, debes irte. Te deseo lo mejor, que seas feliz!")
            6 -> println(colorPredeterminat + "Por fin ! Te gusta vivir al límite, de los que aprueban con un 5. Pero, al fin y al cabo aprobado.")
        }
        println()
        println("Te has ganado el título de ganador y lo valoro. Pero lo difícil no es llegar a la cima, es mantenerse.")
        println("Que me dices ? Te atreves a jugar de nuevo ?")
        print("Si es así, escribe "); print(yellow + "SI");print(colorPredeterminat + ". En cambio, si quieres irte con la victoria en el bolsillo escribe "); println(blue + "NO")
        while (retryChar == false){
            val sc = Scanner(System.`in`)
            val choose = sc.next()
            if(choose.uppercase() == "SI"){
                ronda++
                retryChar= true
                break
            }
            else if (choose.uppercase() == "NO") {
                retryChar= true
                retry = false
            }
            else {
                println("Lo tuyo no es la comprension lectora verdad ? Crees que $choose es una opción ? En serio?");
                println("Vuelve a intentarlo...")
            }
        }
    }
    else {
        print(colorPredeterminat + "Vaya... lo siento, pero la respuesta correcta era ");print(green + word);print(
            colorPredeterminat + " :/..."
        );print(cyan + "HAS PERDIDO !!");print(colorPredeterminat + " o como dijo Thomas Alva Edison");println(
            yellow + " «No he fracasado, he encontrado 10.000 soluciones que no funcionan»."
        );println(colorPredeterminat + "Mmmm ya, lo sé. No te ha animado mucho esa frase. Te diré una mejor, atento. Mi profesor de programación dijo una vez: ");println(
            yellow + "HABER ESTUDIAO!"
        )
        println()
        println(colorPredeterminat+ "No ha sido tu mejor partida... pero no te preocupes, quieres intentarlo de nuevo ?")
        print("Si es así, escribe "); print(yellow + "SI");print(colorPredeterminat + ". En cambio, si quieres irte como un perdedor escribe "); println(blue + "NO")
        println(colorPredeterminat + "La eleccion es tuya... que me dices ?")
        while (retryChar == false){
            val sc = Scanner(System.`in`)
            val choose = sc.next()
            if(choose.uppercase() == "SI"){
                ronda++
                retryChar= true
                break
            }
            else if (choose.uppercase() == "NO") {
                retryChar= true
                retry = false
            }
            else {
                println("Lo tuyo no es la comprension lectora verdad ? Crees que $choose es una opción ? En serio?");
                println("Vuelve a intentarlo...")
            }
        }
    }
    return retry
}